<?php defined('BASEPATH') or exit('No direct script access allowed');

Class M_chitchat extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->db->query("SET time_zone='+07:00'");
	}

    function listChat($from, $to)
    {
        return $chats = $this->db
                ->from('chat')
                ->where("(chat_from = ". $from ." AND chat_to = ". $to .")")
                ->or_where("(chat_to = ". $from ." AND chat_from = ". $to .")")
                ->order_by('chat.created_at', 'ASC')
                ->limit(100)
                ->get()
                ->result_array();
    }

    /* LAST */
    function lastChat($me)
    {
        return $chats = $this->db
                ->from('chat')
                ->where("chat_from", $me)
                ->or_where("chat_to", $me)
                ->order_by('created_at', 'DESC')
                ->limit(1)
                ->get()
                ->result_array();
    }

    /*  QUERY INI NGGAK YAKIN BENER */
    function listRecipientForDosen($me)
    {
    	return $chats = $this->db
                ->from('chat')
                ->join('user', 'user.usr_id = chat.chat_from', 'LEFT')
                ->join('mahasiswa', 'user.username = mahasiswa.nim', 'LEFT')
                ->where("level", "mhs")
                ->where("chat_from", $me)
                ->or_where("chat_to", $me)
                ->order_by('created_at', 'DESC')
                ->group_by('mahasiswa.nim')
                ->get()
                ->result_array();
    }
}
