<!--outter-wp-->
					<div class="outter-wp">
						<!--sub-heard-part-->
						<div class="sub-heard-part">
							<ol class="breadcrumb m-b-0">
								<li><a href="<?php echo site_url('mhs_home')?>">Home</a></li>
								<li class="active">Chat</li>
							</ol>
						</div>
						<hr>
						<!--//sub-heard-part-->
						<!--Konten Utama-->	
						<div class="col-md-12">
              <?php if ($this->session->flashdata('warning')) {?>
              <div class="alert alert-warning alert-dismissible" role="alert">
                <span aria-hidden="true" data-dismiss="alert" >&times;</span>
                &nbsp;&nbsp;
                <strong>Warning!</strong> <?php echo $this->session->flashdata('warning')?>
              </div>
              <?php } ?>
              <?php if ($this->session->flashdata('success')) {?>
              <div class="alert alert-success alert-dismissible" role="alert">
                <span aria-hidden="true" data-dismiss="alert" >&times;</span>
                &nbsp;&nbsp;
                <strong>Success!</strong> <?php echo $this->session->flashdata('success')?>
              </div>
              <?php } ?>

              <div class="messaging">
                <div class="inbox_msg">
                  <div class="inbox_people">
                    <div class="headind_srch">
                      <div class="recent_heading">
                        <h4>Mahasiswa</h4>
                      </div>
                      <div class="srch_bar">
                        <div class="stylish-input-group">
                          <!-- <input type="text" class="search-bar"  placeholder="Search" > -->
                          <!-- <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button> -->
                        </div>
                      </div>
                    </div>
                    <div class="inbox_chat">
                        
                        <?php if ($recipient) { ?>
                          <?php foreach ($recipient as $key => $value) { ?>
                            <a href="<?php echo site_url('chat/to/'.$value['username']) ?>">
                              <div class="chat_list <?php if ($value['flag'] == 1) { echo "active_chat"; } ?> ">
                                <div class="chat_people">
                                  <?php $photo =  base_url().'assets/foto_profil/mahasiswa/'.$value['foto']; ?>
                                  <div class="chat_img"> 
                                    <img src="<?php echo $photo ?>" alt="mhs"> 
                                  </div>
                                  <div class="chat_ib">
                                    <h5><?php echo $value['nama'] ?> 
                                      <span class="chat_date"> 
                                        <?php echo (!empty($value['last_chat'])) ? date('M d', strtotime($value['last_chat'][0]['created_at'])) : "" ?> 
                                        <?php if (!empty($value['last_chat'])) {  if ($value['last_chat']['0']['chat_read'] == 0) { ?>
                                          <i class="fa fa-circle"></i>
                                        <?php } }  ?>
                                      </span> 
                                    </h5>
                                    <p> <?php echo (!empty($value['last_chat'])) ? $value['last_chat'][0]['chat_content'] : ""; ?> </p>
                                  </div>
                                </div>
                              </div>
                            </a>
                          <?php } ?>
                        <?php } ?>
                      
                    </div>
                  </div>
                  <div class="mesgs">
                    <div class="msg_history">
                    <?php if (!empty($chat)) { ?>
                      <?php foreach ($chat as $key => $value) { ?>
                        
                        <?php if ($value['chat_to'] == $this->session->userdata('usr_id')) { ?>
                        <div class="incoming_msg">
                          <div class="incoming_msg_img"> 
                            <?php if (isset($photo)) { ?>
                              <img src="<?php echo $photo ?>" alt="mhs"> 
                            <?php } else {?>
                              <img src="https://ptetutorials.com/images/user-profile.png" alt="dosen"> 
                            <?php } ?>
                          </div>
                          <div class="received_msg">
                            <div class="received_withd_msg">
                              <p><?php echo $value['chat_content'] ?></p>
                              <span class="time_date">  <?php echo date("h:i A", strtotime($value['created_at'])) ?>  |    <?php echo date("M d", strtotime($value['created_at'])) ?> </span></div>
                          </div>
                        </div>
                        <?php } else { ?>
                        <div class="outgoing_msg">
                          <div class="sent_msg">
                            <p><?php echo $value['chat_content'] ?></p>
                            <span class="time_date">  <?php echo date("h:i A", strtotime($value['created_at'])) ?>  |    <?php echo date("M d", strtotime($value['created_at'])) ?> </span></div>
                        </div>
                        <?php } ?>
                      <?php } ?>
                    <?php } ?>
                    </div>
                    <div class="type_msg">
                      <form action="<?php echo base_url(); ?>chat/send" method="POST">
                        <div class="input_msg_write">
                          <input type="hidden" name="username" value="<?php echo $sendTo ?>">
                          <input type="hidden" name="chat[chat_to]" value="<?php echo $to ?>">
                          <input type="hidden" name="chat[chat_from]" value="<?php echo $from ?>">
                          <input type="text" name="chat[chat_content]" class="write_msg" placeholder="Type a message" />
                          <button class="msg_send_btn" type="button">
                            <i class="fa fa-chevron-circle-right"></i>
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <p class="text-center top_spac"><small> Design by Sunil Rajput </small> </p>
              </div>
						</div>	
					</div>
				</div>
			</div>
			<!--Konten Utama-->